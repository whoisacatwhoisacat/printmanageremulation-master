package ru.cbr.whoisacat.pme;

import java.rmi.RemoteException;
import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

class PrintManagerEmulationTest{

    public static void main(String[] args) throws RemoteException{
        IPrintManagerEmulation pme = new PrintManagerEmulation();
        ((PrintManagerEmulation) pme).go();

        DocType[] dta = {new DocType("type 1",15,DocType.PaperSize.A3),
                new DocType("type 2",13,DocType.PaperSize.A4),
                new DocType("type 3",7,DocType.PaperSize.A5),
                new DocType("type 4",28,DocType.PaperSize.A3)};
        MyDoc[] mda = new MyDoc[500];
        int i = 0;
        for(MyDoc md : mda){
            mda[i] = new MyDoc(dta[(int) (Math.random() * 4)],"doc " + i,
                           " content of doc " + i);
            i++;
        }
        for(i = 0; i < 200; i++){
            pme.addToPrintQueue(mda[i]);
            try{
                Thread.sleep(50);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            if(i == 102){
                System.out.println("average" + pme.getAveragePrintingTime());
            }
        }
        System.out.println("stop printing:");
        LinkedList<MyDoc> restOfQueue = pme.stopPrinting();
        for(MyDoc doc : restOfQueue){
            System.out.println(doc.getName());
            System.out.println(doc.getContent());
        }
        for(i = 200; i < 400; i++){
            pme.addToPrintQueue(mda[i]);
            try{
                Thread.sleep(5);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            if(i == 202 || i == 280 || i == 390)
                System.out.println("average" + pme.getAveragePrintingTime());
        }
        System.out.println("average" + pme.getAveragePrintingTime());
        try{
            Thread.sleep(707);
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.println("average" + pme.getAveragePrintingTime());
        System.out.println("resume");
        System.out.println("resume = " + pme.resume());
        System.out.println("370 " + pme.removeFromPrintQueue(mda[390]));
        System.out.println("2 " + pme.removeFromPrintQueue(mda[2]));
        System.out.println("490 " + pme.removeFromPrintQueue(mda[490]));
        System.out.println("27 " + pme.removeFromPrintQueue(mda[27]));
        pme.removeFromPrintQueue(mda[390]);

        System.out.println("average" + pme.getAveragePrintingTime());
        try{
            System.out.println(Thread.currentThread().getName() + "Thread.sleep(8700); go");
            Thread.sleep(8700);
            System.out.println(Thread.currentThread().getName() + "Thread.sleep(8700); out");
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        LinkedList<MyDoc> printed = pme.getSortedListOfPrinted(
                IPrintManagerEmulation.SortType.NUMBER_IN_THE_QUEUE);
        LinkedList<MyDoc> docType = pme.getSortedListOfPrinted(
                        IPrintManagerEmulation.SortType.DOC_TYPE);
        LinkedList<MyDoc> printDuration = pme.getSortedListOfPrinted(
                        IPrintManagerEmulation.SortType.PRINT_DURATION);
        LinkedList<MyDoc> paperSize = pme.getSortedListOfPrinted(
                        IPrintManagerEmulation.SortType.PAPER_SIZE);

        System.out.println("printed");
        for(MyDoc doc : printed){
            System.out.println(
                    "doc: " + doc.getName() + " type: " + doc.getDocType().getName() +
                            " print duration: " + doc.getDocType().getPrintDuration() +
                            " paper size: " + doc.getDocType().getPaperSize());
        }
        System.out.println("doc type");
        for(MyDoc doc : docType){
            System.out.println(
                    "doc: " + doc.getName() + " type: " + doc.getDocType().getName() +
                            " print duration: " + doc.getDocType().getPrintDuration() +
                            " paper size: " + doc.getDocType().getPaperSize());
        }
        System.out.println("duration");
        for(MyDoc doc : printDuration){
            System.out.println(
                    "doc: " + doc.getName() + " type: " + doc.getDocType().getName() +
                            " print duration: " + doc.getDocType().getPrintDuration() +
                            " paper size: " + doc.getDocType().getPaperSize());
        }
        System.out.println("size");
        for(MyDoc doc : paperSize){
            System.out.println(
                    "doc: " + doc.getName() + " type: " + doc.getDocType().getName() +
                            " print duration: " + doc.getDocType().getPrintDuration() +
                            " paper size: " + doc.getDocType().getPaperSize());
        }
        System.out.println("average" + pme.getAveragePrintingTime());
    }
}