package ru.cbr.whoisacat.pme;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.LinkedList;

public interface IPrintManagerEmulation extends Remote{

    enum SortType{NUMBER_IN_THE_QUEUE,DOC_TYPE,PRINT_DURATION,PAPER_SIZE,}

    LinkedList<MyDoc> stopPrinting() throws RemoteException;

    void addToPrintQueue(MyDoc doc) throws RemoteException;

    boolean removeFromPrintQueue(MyDoc doc) throws RemoteException;

    LinkedList<MyDoc> getSortedListOfPrinted(SortType sortType) throws RemoteException;

    int getAveragePrintingTime() throws RemoteException;

    int resume() throws RemoteException;
}
