package ru.cbr.whoisacat.pme;

public class MyDoc{

    private DocType mDocType;
    private String mName;
    private String mContent;

    public MyDoc(DocType docType,String name,String content){
        mDocType = docType;
        mName = name;
        mContent = content;
    }

    public DocType getDocType(){
        return mDocType;
    }

    public String getName(){
        return mName;
    }


    public String getContent(){
        return mContent;
    }


    public synchronized void print(){
        try{
            System.out.println();
            System.out.println("printing doc: " + this.getName() + " current time = " +
                                       System.currentTimeMillis() + " print duration: " +
                                       mDocType.getPrintDuration() * 100);
            Thread.sleep((mDocType.getPrintDuration() * 100));
            System.out.println("end of printing. content: " + this.getContent() +
                                       " current time = " + System.currentTimeMillis());
            System.out.println();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
