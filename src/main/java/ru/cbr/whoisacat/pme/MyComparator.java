package ru.cbr.whoisacat.pme;

import java.util.Comparator;

public class MyComparator implements Comparator<MyDoc>{
    IPrintManagerEmulation.SortType mSortType;

    public MyComparator(IPrintManagerEmulation.SortType sortType){
        mSortType = sortType;
    }

    @Override public int compare(MyDoc o1,MyDoc o2){
        switch(mSortType){
            case DOC_TYPE:
                return o1.getDocType().getName().compareTo(o2.getDocType().getName());
            case PAPER_SIZE:
                return o1.getDocType().getPaperSize().compareTo(o2.getDocType().getPaperSize());
            case PRINT_DURATION: default:
                return o1.getDocType().getPrintDuration() - o2.getDocType().getPrintDuration();
        }
    }
}
