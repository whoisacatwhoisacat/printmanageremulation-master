package ru.cbr.whoisacat.pme;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class PrintManagerEmulation implements IPrintManagerEmulation{

    private static final String BINDING_NAME = "ru.cbr.whoisacat.pme.PrintManagerEmulation";
    private Thread mPrintThread;
    LinkedList<MyDoc> mPrintQueue = new LinkedList<>();
    LinkedList<MyDoc> mPrinted = new LinkedList<>();
    private static RunPrinting mRunPrinting;

    public PrintManagerEmulation(){
        mRunPrinting = new RunPrinting(mPrintQueue,mPrinted);
        mPrintThread = new Thread(mRunPrinting);
    }

    public void addToPrintQueue(MyDoc doc){
        mPrintQueue.addLast(doc);
    }

    public LinkedList<MyDoc> stopPrinting(){
        mRunPrinting.setPause(true);
        return mPrintQueue;
    }

    public int resume(){
        int ret = mPrintQueue.size();
        mRunPrinting.setPause(false);
        return ret;
    }

    public boolean removeFromPrintQueue(MyDoc doc){
        return mPrintQueue.remove(doc);
    }

    public LinkedList<MyDoc> getSortedListOfPrinted(SortType sortType){
        if(sortType != SortType.NUMBER_IN_THE_QUEUE)
            Collections.sort(mPrinted,new MyComparator(sortType));
        return new LinkedList<>(mPrinted);
    }

    public int getAveragePrintingTime(){
        long ret = 0;
        ArrayList<MyDoc> list = new ArrayList<>(mPrinted);
        if(list.size() == 0){
            return 0;
        }
        for(MyDoc doc : list){
            ret += doc.getDocType().getPrintDuration() * 100;
        }
        return (int) ret / list.size();
    }

    public static void main(String[] args){

        IPrintManagerEmulation printManagerEmulation = new PrintManagerEmulation();
        ((PrintManagerEmulation) printManagerEmulation).go();
        try{
            System.out.print("Starting registry...");
            Registry registry = LocateRegistry.createRegistry(19877);
            System.out.println(" OK");
            try{
                registry.bind("ClientRegister",printManagerEmulation);
            } catch(AlreadyBoundException abe){
                abe.printStackTrace();
            }
            try{
                System.out.print("Binding service...");
                registry.bind(BINDING_NAME,printManagerEmulation);
                System.out.println(" OK");
            } catch(AlreadyBoundException e){
                e.printStackTrace();
            }
        } catch(RemoteException e){
            e.printStackTrace();
        }
    }

    public void go(){
        mPrintThread.start();
    }
}
