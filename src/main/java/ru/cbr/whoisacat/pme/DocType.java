package ru.cbr.whoisacat.pme;

public class DocType{

    private String name;
    private int printDuration;
    private PaperSize paperSize;

    enum PaperSize {A3,A4,A5}

    public DocType(String name,int printDuration,PaperSize paperSize){
        this.name = name;
        this.printDuration = printDuration;
        this.paperSize = paperSize;
    }

    public String getName(){
        return name;
    }

    public int getPrintDuration(){
        return printDuration;
    }


    public PaperSize getPaperSize(){
        return paperSize;
    }
}
