package ru.cbr.whoisacat.pme;

import java.util.LinkedList;

public class RunPrinting implements Runnable{
    LinkedList<MyDoc> mPrintQueue;
    LinkedList<MyDoc> mPrinted;
    boolean pause;

    public void setPause(boolean pause){
        this.pause = pause;
    }

    public RunPrinting(LinkedList<MyDoc> printQueue,LinkedList<MyDoc> printed){
        this.mPrintQueue = printQueue;
        this.mPrinted = printed;
    }

    @Override public void run(){
        while(true){
            if(!pause){
                if(mPrintQueue.size() != 0){
                    MyDoc doc = mPrintQueue.getFirst();
                    mPrintQueue.removeFirst();
                    mPrinted.addLast(doc);
                    doc.print();
                }else {
                    try{
                        Thread.sleep(1000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }else {
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
